import React from 'react';

import './App.css';
import Menu from './menu.js';
import Entrada from './entradas.js';
import Footer from './footer';
import LatestPost from './latestPost.js';
import Header from './header';

function App() {
    return (
        <div className='container'>
            <Header></Header>
            <div className='blog-content'>
                <Menu></Menu>
                <Entrada></Entrada>
                <LatestPost></LatestPost>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default App;
