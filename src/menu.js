import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyle = makeStyles((theme) => ({
    root: {
        textAlign: 'center',
        backgroundColor: '#C5EDAC',
        padding: 20,
    },
}));
const Menu = (Props) => {
    const classes = useStyle();
    return (
        <div className='item-menu'>
            <List component='nav' subheader='Menu' className={classes.root}>
                <ListItem button divider>
                    <ListItemText primary='Today'></ListItemText>
                </ListItem>
                <ListItem button divider>
                    <ListItemText primary='Yesterday'></ListItemText>
                </ListItem>
                <ListItem button divider>
                    <ListItemText primary='Last week'></ListItemText>
                </ListItem>
                <ListItem button divider>
                    <ListItemText primary='Archieves'></ListItemText>
                </ListItem>
            </List>
        </div>
    );
};

export default Menu;
