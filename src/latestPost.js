import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
const useStyle = makeStyles((theme) => ({
    root: {
        textAlign: 'center',
        backgroundColor: '#C5EDAC',
        padding: 10,
    },
}));
const LatestPost = (Props) => {
    const classes = useStyle();
    return (
        <div className='item-menu'>
            <List
                component='nav'
                subheader='Last Posts'
                className={classes.root}>
                <ListItem button divider>
                    <ListItemText primary='Meeting with supervisor'></ListItemText>
                </ListItem>
                <ListItem button divider>
                    <ListItemText primary='New car!!!'></ListItemText>
                </ListItem>
                <ListItem button divider>
                    <ListItemText primary='Visit my parents'></ListItemText>
                </ListItem>
                <ListItem button divider>
                    <ListItemText primary='Archieves'></ListItemText>
                </ListItem>
            </List>
        </div>
    );
};

export default LatestPost;
