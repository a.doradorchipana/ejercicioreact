import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const useStyle = makeStyles((theme) => ({
    root: {
        backgroundColor: '#99C2A2',
    },
    title: {
        fontSize: 24,
        textAlign: 'center',
    },
}));
const Header = (Props) => {
    const classes = useStyle();
    return (
        <AppBar className={classes.root} position='static'>
            <Toolbar>
                <Typography
                    align='center'
                    className={classes.title}
                    variant='h1'>
                    My blog about my life
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

export default Header;
