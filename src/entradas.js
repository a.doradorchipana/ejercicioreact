import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import { CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Divider } from '@material-ui/core';

const useStyle = makeStyles((theme) => ({
    root: {
        marginBottom: 15,
        minWidth: 300,
    },
    header: {
        backgroundColor: '#99C2A2',
    },
}));
const Entradas = (Props) => {
    const classes = useStyle();
    return (
        <div className='item-article'>
            <Card className={classes.root} variant='outlined'>
                <CardHeader
                    className={classes.header}
                    title='Meeting with my supervisor'
                    subheader='14 Dec 2011'></CardHeader>
                <CardContent>
                    <p>Today Went to the university by bus.</p>
                    <p>I had a met with my PhD supervisor.</p>
                </CardContent>
            </Card>
            <Card className={classes.root} variant='outlined'>
                <CardHeader
                    className={classes.header}
                    title='New car!!!'
                    subheader='12 Dec 2011'></CardHeader>
                <CardContent>
                    <p>
                        Today I bought my new car. It's a Honda Accord and it's
                        reallu nice.
                    </p>
                    <p>I met some friends at a pub.</p>
                </CardContent>
            </Card>
            <Card className={classes.root} variant='outlined'>
                <CardHeader
                    className={classes.header}
                    title='Visit my parents'
                    subheader='10 Dec 2011'></CardHeader>
                <CardContent>
                    <p>
                        Tried to contact my PhD supervisor. He was out of his
                        office.
                    </p>
                    <p>
                        I visited my parents and we had a nice dinner together
                    </p>
                </CardContent>
            </Card>
        </div>
    );
};

export default Entradas;
